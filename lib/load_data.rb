require_relative '../config/environment'

ATTR_TYPE_MAP = {
  0..19  => 'physician',
  20..27 => 'mailing_address',
  28..35 => 'location_address',
  36..42 => 'physician'
}

COLUMN_INDEX_MAP = {
  0 => 'NPI',
  1 => 'Entity Type Code',
  2 => 'Replacement ',
  3 => 'Employer Identification Number (EIN)',
  4 => 'Provider Organization Name (Legal Business Name)',
  5 => 'Provider Last Name (Legal Name)',
  6 => 'Provider First Name',
  7 => 'Provider Middle Name',
  8 => 'Provider Name Prefix Text',
  9 => 'Provider Name Suffix Text',
  10 => 'Provider Credential Text',
  11 => 'Provider Other Organization Name',
  12 => 'Provider Other Organization Name Type Code',
  13 => 'Provider Other Last Name',
  14 => 'Provider Other First Name',
  15 => 'Provider Other Middle Name',
  16 => 'Provider Other Name Prefix Text',
  17 => 'Provider Other Name Suffix Text',
  18 => 'Provider Other Credential Text',
  19 => 'Provider Other Last Name Type Code',
  20 => 'Provider First Line Business Mailing Address',
  21 => 'Provider Second Line Business Mailing Address',
  22 => 'Provider Business Mailing Address City Name',
  23 => 'Provider Business Mailing Address State Name',
  24 => 'Provider Business Mailing Address Postal Code',
  25 => 'Provider Business Mailing Address Country Code (If outside U.S.)',
  26 => 'Provider Business Mailing Address Telephone Number',
  27 => 'Provider Business Mailing Address Fax Number',
  28 => 'Provider First Line Business Practice Location Address',
  29 => 'Provider Second Line Business Practice Location Address',
  30 => 'Provider Business Practice Location Address City Name',
  31 => 'Provider Business Practice Location Address State Name',
  32 => 'Provider Business Practice Location Address Postal Code',
  33 => 'Provider Business Practice Location Address Country Code (If outside U.S.)',
  34 => 'Provider Business Practice Location Address Telephone Number',
  35 => 'Provider Business Practice Location Address Fax Number',
  36 => 'Provider Enumeration Date',
  37 => 'Last Update Date',
  38 => ' Deactivation Reason Code',
  39 => ' Deactivation Date',
  40 => ' Reactivation Date',
  41 => 'Provider Gender Code'
}

PHYSICIAN_MAP = {
  /^NPI$/ => :number,
  /Entity Type Code/ => :enumeration_type,
  /Replacement / => :replacement_npi,
  /Employer Identification Number/ => :empployer_id_number,
  /Provider Organization Name/ => :organization_name,
  /Provider Last Name/ => :last_name,
  /Provider First Name/ => :first_name,
  /Provider Middle Name/ => :middle_name,
  /Provider Name Prefix Text/ => :name_prefix,
  /Provider Name Suffix Text/ => :name_suffix,
  /Provider Credential Text/ => :credential,
  /Provider Enumeration Date/ => :enumeration_date,
  /Last Update Date/ => :last_updated,
  / Deactivation Reason Code/ => :npi_deactivation_reason_code,
  / Deactivation Date/ => :npi_deactivation_date,
  / Reactivation Date/ => :npi_reactivation_date,
  /Provider Gender Code/ => :gender
}

MAILING_ADDRESS_MAP = LOCATION_ADDRESS_MAP = {
  /First Line/   => :address_1,
  /Second Line/  => :address_2,
  /City Name/    => :city,
  /State Name/   => :state,
  /Postal Code/  => :postal_code,
  /Country Code/ => :country_code,
  /Telephone/    => :telephone_number,
  /Fax Number/   => :fax_number
}

class DataLoader
  class << self
    def load_one_line(line)
      physician = Physician.new
      mailing_address  = OfficeAddress.new({address_type: 'DOM', address_purpose: 'MAILING'})
      location_address = OfficeAddress.new({address_type: 'DOM', address_purpose: 'LOCATION'})

      COLUMN_INDEX_MAP.size.times do |idx|
        object_name = ATTR_TYPE_MAP.select { |x| x === idx }.values.first
        object_attribute = csv_column_to_attribute(object_name, idx) || next
        eval(object_name).send("#{object_attribute}=", line[idx])
      end
      
      physician.office_addresses = [find_address(mailing_address), find_address(location_address) ]
      physician.save!
      print('.')
    end # load_one_line

    def csv_column_to_attribute(object_name, index)
      key, value = eval("#{object_name.upcase}_MAP").detect { |k,v| COLUMN_INDEX_MAP[index].match(k) }
      value
    end

    def find_address(address)
      OfficeAddress.where("address_1 = ? AND address_2 = ? AND postal_code = ? AND address_purpose = ?", 
        address.address_1, address.address_2, address.postal_code, address.address_purpose).first || address
    end # find_address

    def load_data
      puts("\nProcessing npidata_small.csv file ...\n")
      file = CSV.open("#{ROOT}/data/npidata_small.csv", 'r')
      file.readline
      file.read.each { |line| load_one_line(line) }
      file.close
      puts("\nFinish processing npidata_small.csv.\n")
    end

  end # self
end

DataLoader.load_data