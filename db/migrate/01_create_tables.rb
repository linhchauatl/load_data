class CreateTables < ActiveRecord::Migration

  def create_physicians
    create_table :physicians do |t|
      t.string :number, index: true
      t.string :status
      t.string :credential
      t.string :first_name, index: true
      t.string :last_name, index: true
      t.string :middle_name
      t.string :name
      t.boolean :sole_proprietor
      t.string :gender
      t.string :name_prefix
      t.string :name_suffix
      t.string :authorized_official_first_name
      t.string :authorized_official_last_name
      t.string :authorized_official_name_prefix
      t.string :authorized_official_title_or_position
      t.string :authorized_official_telephone_number
      t.string :organization_name
      t.string :organizational_subpart
      t.string :enumeration_type
      t.date :enumeration_date
      t.date :last_updated
      t.string :replacement_npi
      t.string :empployer_id_number
      t.string :npi_deactivation_reason_code
      t.date :npi_deactivation_date
      t.date :npi_reactivation_date

      t.timestamps null: false
    end
  end

  def create_office_addresses
    create_table :office_addresses do |t|
      t.string :address_1, index: true
      t.string :address_2, index: true
      t.string :city, index: true
      t.string :state, index: true
      t.string :postal_code, index: true
      t.string :country_code, index: true
      t.string :telephone_number
      t.string :fax_number
      t.string :address_type, index: true
      t.string :address_purpose, index: true

      t.timestamps null: false
    end
  end

  def create_office_addresses_physicians
    create_table :office_addresses_physicians do |t|
      t.references :office_address, index: true, foreign_key: true
      t.references :physician, index: true, foreign_key: true

      t.timestamps null: false
    end
  end

  def up
    create_physicians
    create_office_addresses
    create_office_addresses_physicians
  end

  def down
    drop_table :physicians, force: :cascade
    drop_table :office_addresses, force: :cascade
    drop_table :office_addresses_physicians, force: :cascade
  end

end
