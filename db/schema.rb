# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 1) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "office_addresses", force: :cascade do |t|
    t.string   "address_1"
    t.string   "address_2"
    t.string   "city"
    t.string   "state"
    t.string   "postal_code"
    t.string   "country_code"
    t.string   "telephone_number"
    t.string   "fax_number"
    t.string   "address_type"
    t.string   "address_purpose"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "office_addresses", ["address_1"], name: "index_office_addresses_on_address_1", using: :btree
  add_index "office_addresses", ["address_2"], name: "index_office_addresses_on_address_2", using: :btree
  add_index "office_addresses", ["address_purpose"], name: "index_office_addresses_on_address_purpose", using: :btree
  add_index "office_addresses", ["address_type"], name: "index_office_addresses_on_address_type", using: :btree
  add_index "office_addresses", ["city"], name: "index_office_addresses_on_city", using: :btree
  add_index "office_addresses", ["country_code"], name: "index_office_addresses_on_country_code", using: :btree
  add_index "office_addresses", ["postal_code"], name: "index_office_addresses_on_postal_code", using: :btree
  add_index "office_addresses", ["state"], name: "index_office_addresses_on_state", using: :btree

  create_table "office_addresses_physicians", force: :cascade do |t|
    t.integer  "office_address_id"
    t.integer  "physician_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "office_addresses_physicians", ["office_address_id"], name: "index_office_addresses_physicians_on_office_address_id", using: :btree
  add_index "office_addresses_physicians", ["physician_id"], name: "index_office_addresses_physicians_on_physician_id", using: :btree

  create_table "physicians", force: :cascade do |t|
    t.string   "number"
    t.string   "status"
    t.string   "credential"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "middle_name"
    t.string   "name"
    t.boolean  "sole_proprietor"
    t.string   "gender"
    t.string   "name_prefix"
    t.string   "name_suffix"
    t.string   "authorized_official_first_name"
    t.string   "authorized_official_last_name"
    t.string   "authorized_official_name_prefix"
    t.string   "authorized_official_title_or_position"
    t.string   "authorized_official_telephone_number"
    t.string   "organization_name"
    t.string   "organizational_subpart"
    t.string   "enumeration_type"
    t.date     "enumeration_date"
    t.date     "last_updated"
    t.string   "replacement_npi"
    t.string   "empployer_id_number"
    t.string   "npi_deactivation_reason_code"
    t.date     "npi_deactivation_date"
    t.date     "npi_reactivation_date"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  add_index "physicians", ["first_name"], name: "index_physicians_on_first_name", using: :btree
  add_index "physicians", ["last_name"], name: "index_physicians_on_last_name", using: :btree
  add_index "physicians", ["number"], name: "index_physicians_on_number", using: :btree

  add_foreign_key "office_addresses_physicians", "office_addresses"
  add_foreign_key "office_addresses_physicians", "physicians"
end
