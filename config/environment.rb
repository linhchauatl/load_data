require 'pg'
require 'active_record'
require 'erb'
require 'elasticsearch/model'
require 'csv'

ROOT = File.expand_path '../..', __FILE__

Dir.glob('app/**/*.rb').each { |file| require_relative "../#{file}" }
Dir.glob('db/migrate/**/*.rb').each { |file| require_relative "../#{file}" }


include ActiveRecord::Tasks

class Seeder
  def initialize(seed_file)
    @seed_file = seed_file
  end

  def load_seed
    raise "Seed file '#{@seed_file}' does not exist" unless File.file?(@seed_file)
    load @seed_file
  end
end

ENVIRONMENT = ENV['ENV'] || ENV['RAILS_ENV'] || ENV['RACK_ENV'] || 'development'

DatabaseTasks.env = ENVIRONMENT
DatabaseTasks.database_configuration = YAML.load(ERB.new(File.read(File.join(ROOT, 'config/database.yml'))).result)
DatabaseTasks.db_dir = File.join ROOT, 'db'
DatabaseTasks.fixtures_path = File.join ROOT, 'test/fixtures'
DatabaseTasks.migrations_paths = [File.join(ROOT, 'db/migrate')]
DatabaseTasks.seed_loader = Seeder.new File.join ROOT, 'db/seeds.rb'
DatabaseTasks.root = ROOT

ActiveRecord::Base.configurations = DatabaseTasks.database_configuration
ActiveRecord::Base.establish_connection DatabaseTasks.env.to_sym