class OfficeAddressesPhysician < ActiveRecord::Base
  belongs_to :office_address
  belongs_to :physician
end
