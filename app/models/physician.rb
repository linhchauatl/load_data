class Physician < ActiveRecord::Base
  include Elasticsearch::Model

  has_many :office_addresses_physicians, dependent: :destroy
  has_many :office_addresses, through: :office_addresses_physicians

  def full_name
    return organization_name if enumeration_type == '2'
    return "#{first_name} #{last_name}"
  end

  def npi_type
    "NPI-#{enumeration_type}"
  end
end
