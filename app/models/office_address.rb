class OfficeAddress < ActiveRecord::Base
  include Elasticsearch::Model

  has_many :office_addresses_physicians, dependent: :destroy
  has_many :physicians, through: :office_addresses_physicians
end
