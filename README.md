## load_data

Ruby script that loads NPI data in a CSV file into PostgreSQL database

1. Download and setup the script

**Prerequisite:** You must have Ruby 2.3.0 and PostgreSQL installed.

```bash
git clone https://github.com/linhchauatl/load_data.git
# You must modify the file config/database.yml to match with your PostgreSQL setup.

gem install bundler
bundle
rake db:create db:migrate
```

2. Run the script

```bash
ruby lib/load_data.rb
```

